package ru.voroj.practice.shop.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.voroj.practice.shop.dto.CustomerDTO;
import ru.voroj.practice.shop.dto.OrderDTO;
import ru.voroj.practice.shop.dto.ProductDTO;

import java.util.List;
import java.util.Random;

@Service
public class SchedulerService {
    private static final Logger log = Logger.getLogger(SchedulerService.class);
    private final OrderService orderService;
    private final ProductService productService;
    private final CustomerService customerService;
    private final Random random = new Random();

    @Autowired
    public SchedulerService(OrderService orderService, ProductService productService, CustomerService customerService) {
        this.orderService = orderService;
        this.productService = productService;
        this.customerService = customerService;
    }

    @Scheduled(fixedRate = 5000)
    public void createOrder() {
        OrderDTO order = new OrderDTO();
        List<ProductDTO> products = productService.findAll();
        List<CustomerDTO> customers = customerService.findAll();

        order.setCustomerId(random.nextInt(customers.size()) + 1);
        order.setProductId(random.nextInt(products.size()) + 1);
        orderService.create(order);

        log.info("Успешно (заказ с customerId = " + order.getCustomerId()
                + " и productId = " + order.getProductId() + " создан)");
    }
}
