package ru.voroj.practice.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.voroj.practice.shop.dto.ProductDTO;
import ru.voroj.practice.shop.mapper.ProductMapper;
import ru.voroj.practice.shop.model.Product;
import ru.voroj.practice.shop.repositorie.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Autowired
    public ProductService(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public ProductDTO create(ProductDTO productDTO) {
        Product product = productMapper.productDTOtoProduct(productDTO);
        productRepository.save(product);

        return productMapper.productToProductDTO(product);
    }

    public List<ProductDTO> findAll() {
        List<Product> products = productRepository.findAll();

        return products.stream().map(productMapper::productToProductDTO).collect(Collectors.toList());
    }

    public ProductDTO findById(long id) {
        return productMapper.productToProductDTO(productRepository.getById(id));
    }

    public ProductDTO update(ProductDTO productDTO, long id) {
        Product product = productMapper.productDTOtoProduct(productDTO);
        product.setId(id);
        productRepository.save(product);

        return productMapper.productToProductDTO(product);
    }

    public void delete(long id) {
        productRepository.deleteById(id);
    }
}
