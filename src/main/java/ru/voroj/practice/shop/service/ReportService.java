package ru.voroj.practice.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.voroj.practice.shop.repositorie.OrderRepository;

import java.util.List;

@Service
public class ReportService {
    private final OrderRepository orderRepository;

    @Autowired
    public ReportService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Long> showReportCustomer(long id) {
        return orderRepository.showReportCustomer(id);
    }

    public List<Long> showReportProduct(long id) {
        return orderRepository.showReportProduct(id);
    }
}
