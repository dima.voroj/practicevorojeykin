package ru.voroj.practice.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.voroj.practice.shop.dto.OrderDTO;
import ru.voroj.practice.shop.mapper.OrderMapper;
import ru.voroj.practice.shop.model.Order;
import ru.voroj.practice.shop.repositorie.CustomerRepository;
import ru.voroj.practice.shop.repositorie.OrderRepository;
import ru.voroj.practice.shop.repositorie.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final OrderMapper orderMapper;

    @Autowired
    public OrderService(OrderRepository orderRepository, CustomerRepository customerRepository,
                        ProductRepository productRepository, OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.orderMapper = orderMapper;
    }

    public OrderDTO create(OrderDTO orderDTO) {
        Order order = orderMapper.orderDTOtoOrder(orderDTO);
        orderRepository.save(order);

        return orderMapper.orderToOrderDTO(order);
    }

    public List<OrderDTO> findAll() {
        List<Order> orders = orderRepository.findAll();

        return orders.stream().map(orderMapper::orderToOrderDTO).collect(Collectors.toList());
    }

    public OrderDTO findById(long id) {
        return orderMapper.orderToOrderDTO(orderRepository.getById(id));
    }

    public OrderDTO update(long id, OrderDTO orderDTO) {
        Order order = orderMapper.orderDTOtoOrder(orderDTO);
        order.setId(id);
        orderRepository.save(order);

        return orderMapper.orderToOrderDTO(order);
    }

    public void delete(long id) {
        orderRepository.deleteById(id);
    }
}
