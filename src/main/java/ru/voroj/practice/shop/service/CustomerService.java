package ru.voroj.practice.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.voroj.practice.shop.dto.CustomerDTO;
import ru.voroj.practice.shop.mapper.CustomerMapper;
import ru.voroj.practice.shop.model.Customer;
import ru.voroj.practice.shop.repositorie.CustomerRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    @Autowired
    public CustomerService(CustomerRepository customerRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    public CustomerDTO create(CustomerDTO customerDTO) {
        Customer customer = customerMapper.customerDTOtoCustomer(customerDTO);
        customerRepository.save(customer);

        return customerMapper.customerToCustomerDTO(customer);
    }

    public List<CustomerDTO> findAll() {
        List<Customer> customers = customerRepository.findAll();

        return customers.stream().map(customerMapper::customerToCustomerDTO).collect(Collectors.toList());
    }

    public CustomerDTO findById(long id) {
        return customerMapper.customerToCustomerDTO(customerRepository.getById(id));
    }

    public CustomerDTO update(CustomerDTO customerDTO, long id) {
        Customer customer = customerMapper.customerDTOtoCustomer(customerDTO);
        customer.setId(id);
        customerRepository.save(customer);

        return customerMapper.customerToCustomerDTO(customer);
    }

    public void delete(long id) {
        customerRepository.deleteById(id);
    }
}
