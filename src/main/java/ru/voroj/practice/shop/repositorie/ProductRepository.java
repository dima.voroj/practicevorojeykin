package ru.voroj.practice.shop.repositorie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import ru.voroj.practice.shop.model.Product;

@Component
public interface ProductRepository extends JpaRepository<Product, Long> {
}
