package ru.voroj.practice.shop.repositorie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import ru.voroj.practice.shop.model.Order;

import java.util.List;

@Component
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("select o.id from Order o, Customer c, Product p where o.customerId = :id and c.id = :id and  o.productId = p.id")
    List<Long> showReportCustomer(@Param(value = "id") long id);

    @Query("select o.id from Order o, Customer c, Product p where o.productId= :id and p.id = :id and  o.customerId = c.id")
    List<Long> showReportProduct(@Param(value = "id") long id);
}
