package ru.voroj.practice.shop.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.voroj.practice.shop.dto.ProductDTO;
import ru.voroj.practice.shop.service.ProductService;

import java.util.List;

@RestController
public class ProductController {
    private final ProductService productService;
    private static final Logger log = Logger.getLogger(OrderController.class);

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Operation(summary = "Добавление товара в БД")
    @PostMapping(value = "/products")
    public ResponseEntity<ProductDTO> create(@RequestBody @Parameter(description = "Товар") ProductDTO product) {
        ProductDTO productNew = productService.create(product);

        log.info("Успешно (продукт с id = " + product.getId() + " создан)");
        return new ResponseEntity<>(productNew, HttpStatus.CREATED);
    }

    @Operation(summary = "Получение списка всех товаров")
    @GetMapping(value = "/products")
    public ResponseEntity<List<ProductDTO>> read() {
        List<ProductDTO> products = productService.findAll();

        log.info("Успешно (полчение списка всех продуктов)");
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @Operation(summary = "Получение сведений о товаре по id")
    @GetMapping(value = "/products/{id}")
    public ResponseEntity<ProductDTO> read(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") int id) {
        ProductDTO product = productService.findById(id);

        log.info("Успешно (продукт с id = " + product.getId() + " найден)");
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @Operation(summary = "Изменение данных о товаре по id")
    @PutMapping(value = "/products/{id}")
    public ResponseEntity<ProductDTO> update(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") int id,
                                    @RequestBody @Parameter(description = "Товар") ProductDTO product) {
        ProductDTO productUpdate = productService.update(product, id);

        log.info("Успешно (продукт с id = " + product.getId() + " обновлен)");
        return new ResponseEntity<>(productUpdate, HttpStatus.OK);

    }

    @Operation(summary = "Удаление товара по id")
    @DeleteMapping(value = "/products/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") int id) {
        productService.delete(id);

        log.info("Успешно (продукт с id = " + id + " удален)");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
