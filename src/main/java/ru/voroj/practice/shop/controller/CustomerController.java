package ru.voroj.practice.shop.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.voroj.practice.shop.dto.CustomerDTO;
import ru.voroj.practice.shop.service.CustomerService;

import java.util.List;

@RestController
public class CustomerController {
    private final CustomerService customerService;
    private static final Logger log = Logger.getLogger(OrderController.class);

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Operation(summary = "Добавление пользователя в БД")
    @PostMapping(value = "/customers")
    public ResponseEntity<CustomerDTO> create(@RequestBody @Parameter(description = "Покупатель") CustomerDTO customer) {
        CustomerDTO customerNew = customerService.create(customer);

        log.info("Успешно (покупатель с id = " + customer.getId() + " создан)");
        return new ResponseEntity<>(customerNew, HttpStatus.CREATED);
    }

    @Operation(summary = "Получение списка всех покупателей")
    @GetMapping(value = "/customers")
    public ResponseEntity<List<CustomerDTO>> read() {
        List<CustomerDTO> customers = customerService.findAll();

        log.info("Успешно (покупатель списка всех покупателей)");
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @Operation(summary = "Получение сведений о покупателе по id")
    @GetMapping(value = "/customers/{id}")
    public ResponseEntity<CustomerDTO> read(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") int id) {
        CustomerDTO customer = customerService.findById(id);

        log.info("Успешно (покупатель с id = " + customer.getId() + " найден)");
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @Operation(summary = "Изменение данных покупателя по id")
    @PutMapping(value = "/customers/{id}")
    public ResponseEntity<CustomerDTO> update(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") int id,
                                    @RequestBody @Parameter(description = "Покупатель") CustomerDTO customer) {
        CustomerDTO customerUpdate = customerService.update(customer, id);

        log.info("Успешно (покупатель с id = " + customer.getId() + " обновлен)");
        return new ResponseEntity<>(customerUpdate, HttpStatus.OK);
    }

    @Operation(summary = "Удаление пользователя по id")
    @DeleteMapping(value = "/customers/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") int id) {
        customerService.delete(id);

        log.info("Успешно (покупатель с id = " + id + " удален)");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
