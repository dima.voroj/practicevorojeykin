package ru.voroj.practice.shop.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.voroj.practice.shop.dto.OrderDTO;
import ru.voroj.practice.shop.service.OrderService;
import ru.voroj.practice.shop.service.ReportService;
import ru.voroj.practice.shop.service.SchedulerService;

import java.util.List;


@RestController
public class OrderController {
    private final OrderService orderService;
    private final SchedulerService schedulerService;
    private final ReportService reportService;
    private static final Logger log = Logger.getLogger(OrderController.class);

    @Autowired
    public OrderController(OrderService orderService, SchedulerService schedulerService, ReportService reportService) {
        this.orderService = orderService;
        this.schedulerService = schedulerService;
        this.reportService = reportService;
    }

    @Operation(summary = "Добавление заказа в БД")
    @PostMapping(value = "/orders")
    public ResponseEntity<OrderDTO> create(@RequestBody @Parameter(description = "Заказ") OrderDTO order) {
        OrderDTO orderNew = orderService.create(order);

        log.info("Успешно (заказ с customerId = " + orderNew.getCustomerId()
                + " и productId = " + orderNew.getProductId() + " создан)");
        return new ResponseEntity<>(orderNew, HttpStatus.CREATED);
    }

    @Operation(summary = "Получение списка всех заказов")
    @GetMapping(value = "/orders")
    public ResponseEntity<List<OrderDTO>> findAll() {
        List<OrderDTO> orders = orderService.findAll();

        log.info("Успешно (получение списка всех заказов)");
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @Operation(summary = "Получение сведений о заказе по id")
    @GetMapping(value = "/orders/{id}")
    public ResponseEntity<OrderDTO> findById(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") long id) {
        OrderDTO order = orderService.findById(id);

        log.info("Успешно (получение заказа с id: " + id + ")");
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @Operation(summary = "Изменение данных заказа по id")
    @PutMapping(value = "/orders/{id}")
    public ResponseEntity<OrderDTO> update(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") long id,
                                           @RequestBody @Parameter(description = "Заказ") OrderDTO order) {
        OrderDTO orderUpdate = orderService.update(id, order);

        log.info("Успешно (изменение заказа с id: " + id + ")");
        return new ResponseEntity<>(orderUpdate, HttpStatus.OK);
    }

    @Operation(summary = "Удаление заказа по id")
    @DeleteMapping(value = "/orders/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") long id) {
        orderService.delete(id);

        log.info("Успешно (удаление заказа с id: " + id + ")");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Получение списка всех заказов, сделанных пользователем по его id")
    @GetMapping(value = "/orders/report/customers/{id}")
    public ResponseEntity<List<Long>> showReportCustomer(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") long id) {
        log.info("Успешно (получение списка всех заказов, сделанных пользователем с id: " + id + ")");
        return new ResponseEntity<>(reportService.showReportCustomer(id), HttpStatus.OK);
    }

    @Operation(summary = "Получение списка всех заказов продукта по его id")
    @GetMapping(value = "/orders/report/products/{id}")
    public ResponseEntity<List<Long>> showReportProduct(@PathVariable(name = "id") @Parameter(description = "Уникальный идентификатор") long id) {
        log.info("Успешно (получение списка всех заказов продуктов с id: " + id + ")");
        return new ResponseEntity<>(reportService.showReportProduct(id), HttpStatus.OK);
    }
}
