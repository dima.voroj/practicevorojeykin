package ru.voroj.practice.shop.mapper;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;
import ru.voroj.practice.shop.dto.CustomerDTO;
import ru.voroj.practice.shop.model.Customer;

@Component
@Mapper(componentModel = "spring")
public class CustomerMapper {

    public CustomerDTO customerToCustomerDTO(Customer customer) {
        if (customer == null) {
            return null;
        }

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(customer.getId());
        customerDTO.setName(customer.getName());
        customerDTO.setPhone(customer.getPhone());
        customerDTO.setEmail(customer.getEmail());

        return customerDTO;
    }

    public Customer customerDTOtoCustomer(CustomerDTO customerDTO) {
        if (customerDTO == null) {
            return null;
        }

        Customer customer = new Customer();
        customer.setId(customerDTO.getId());
        customer.setName(customerDTO.getName());
        customer.setPhone(customerDTO.getPhone());
        customer.setEmail(customerDTO.getEmail());

        return customer;
    }
}
