package ru.voroj.practice.shop.mapper;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;
import ru.voroj.practice.shop.dto.OrderDTO;
import ru.voroj.practice.shop.model.Order;

@Component
@Mapper(componentModel = "spring")
public class OrderMapper {

    public OrderDTO orderToOrderDTO(Order order) {
        if (order == null) {
            return null;
        }

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(order.getId());
        orderDTO.setCustomerId(order.getCustomerId());
        orderDTO.setProductId(order.getProductId());

        return orderDTO;
    }

    public Order orderDTOtoOrder(OrderDTO orderDTO) {
        if (orderDTO == null) {
            return null;
        }

        Order order = new Order();
        order.setId(orderDTO.getId());
        order.setCustomerId(orderDTO.getCustomerId());
        order.setProductId(orderDTO.getProductId());

        return order;
    }
}
