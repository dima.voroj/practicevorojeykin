package ru.voroj.practice.shop.mapper;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;
import ru.voroj.practice.shop.dto.ProductDTO;
import ru.voroj.practice.shop.model.Product;

@Component
@Mapper(componentModel = "spring")
public class ProductMapper {
    public ProductDTO productToProductDTO(Product product) {
        if (product == null) {
            return null;
        }

        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setPrice(product.getPrice());

        return productDTO;
    }

    public Product productDTOtoProduct(ProductDTO productDTO) {
        if (productDTO == null) {
            return null;
        }

        Product product = new Product();
        product.setId(productDTO.getId());
        product.setName(productDTO.getName());
        product.setPrice(productDTO.getPrice());

        return product;
    }
}
