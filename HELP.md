###Скрипт для создания таблиц
```postgresql
CREATE TABLE customers
(
    id    BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name  varchar,
    phone varchar,
    email varchar
);

CREATE TABLE products
(
    id    BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name  varchar,
    price double precision
);

CREATE TABLE orders
(
    id          BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    customer_id BIGINT,
    product_id  BIGINT,
    FOREIGN KEY (customer_id) REFERENCES customers (id) ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE
);
```
###Тестовые данные
```postgresql
INSERT INTO products
VALUES (DEFAULT, 'Samsung', 20000);
INSERT INTO products
VALUES (DEFAULT, 'Nokia', 18000);
INSERT INTO products
VALUES (DEFAULT, 'Huawei', 25000);
INSERT INTO products
VALUES (DEFAULT, 'LG', 22000);
INSERT INTO products
VALUES (DEFAULT, 'Motorola', 17000);

INSERT INTO customers
VALUES (DEFAULT, 'Nik', '89023458976', 'nuk@mail.ru');
INSERT INTO customers
VALUES (DEFAULT, 'Maks', '89567345612', 'maks@mail.ru');
INSERT INTO customers
VALUES (DEFAULT, 'Denis', '89576543908', 'denis@mail.ru');
INSERT INTO customers
VALUES (DEFAULT, 'Alina', '892487408712', 'alina@mail.ru');
INSERT INTO customers
VALUES (DEFAULT, 'Oleg', '89375698734', 'oleg@mail.ru');
```
